﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Form1 : Form
    {
        private TcpClient _client;
        
        private NetworkStream _nwStream;
        private Thread _thread ;
        Boolean listen = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //connect to the server
            if (this.txtRemoteIP.Text == "")
                return;
            if (this.txtRemoteHostName.Text == "")
                return;
            if(this.txtRemotePort.Text == "")
                return;

            _client= new TcpClient();
            _client.Connect(IPAddress.Parse(this.txtRemoteIP.Text),Convert.ToInt32(this.txtRemotePort.Text));
            // send a message to server
            if(_client.Connected)
            {
                _nwStream = _client.GetStream();
                byte[] msg = Encoding.ASCII.GetBytes("Hi LAGARİ");
                _nwStream.Write(msg, 0, msg.Length);    
                this.groupBox2.Enabled= false;
                // _client.Close();

                _thread = new Thread(listening);
                _thread.Start();
                listen = true;
            }
        
        
        
        }

        private void listening()
        {
            while (listen)
            {
                if (_client.Available > 0)
                {
                    byte[] msg = new byte[_client.ReceiveBufferSize];
                    _nwStream.Read(msg, 0, msg.Length);

                    string _msg = Encoding.ASCII.GetString(msg);
                    this.listBox1.Items.Add(_msg);
                }
            }
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // _client = new TcpClient();
            // _client.Connect(IPAddress.Parse(this.txtRemoteIP.Text), Convert.ToInt32(this.txtRemotePort.Text));
            // send a message to server
            if (_client.Connected)
            {
                _nwStream = _client.GetStream();
                byte[] msg = Encoding.ASCII.GetBytes(this.txtSend.Text);
                _nwStream.Write(msg, 0, msg.Length);
                // this.groupBox2.Enabled = false;
               // _client.Close();
            }
        }
    }
}
