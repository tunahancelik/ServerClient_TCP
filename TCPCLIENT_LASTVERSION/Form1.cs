﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace TCPCLIENT_LASTVERSION
{
    public partial class Form1 : Form
    {
        private TcpListener _server;
        private TcpClient _client;
        private NetworkStream _nStream;
        private Thread _thread;
        Boolean _accept = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _server = new TcpListener(IPAddress.Parse(this.txtIPAddress.Text),Convert.ToInt32(this.txtPort.Text));
            _server.Start();
            _client = _server.AcceptTcpClient();
            _nStream = _client.GetStream();
            
            this.groupBox1.Enabled = false;
            this.button1.Enabled = false;
            this.button2.Enabled = true;

            _thread = new Thread(AcceptClientSystem);
            _thread.Start();
            _accept= true;
        
        }

        private void AcceptClientSystem()
        {
            while (_accept)
            {
                if(_client.Available > 0) 
                        {
                    _nStream =_client.GetStream();
                    //receive the message
                    byte[] msg = new byte[_client.ReceiveBufferSize];
                    _nStream.Read(msg, 0, msg.Length);
                    _ = Encoding.ASCII.GetString(msg);

                    //show the message
                    this.listBox1.Items.Add(msg.ToString());


                }


            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false; 

            IPHostEntry _host = Dns.GetHostEntry(Dns.GetHostName());

            this.txtIPAddress.Text = _host.AddressList[1].ToString();
            this.txtHostName.Text =  _host.HostName.ToString();
            this.txtPort.Text = "11000";

            this.groupBox1.Enabled= true;
            this.button1.Enabled= true;
            this.button2.Enabled= false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            _thread.Abort();
            _server.Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            byte[] msg = Encoding.ASCII.GetBytes(this.txtSend.Text);
            _nStream.Write(msg, 0, msg.Length); 

        }
    }
}
